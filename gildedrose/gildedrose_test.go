package gildedrose_test

import (
	"testing"

	"github.com/emilybache/gildedrose-refactoring-kata/gildedrose"
)

func Test_Stub(t *testing.T) {
	if 1 != 1 {
		t.Errorf("Test Stub Failed")
	}
}

func Test_Foo(t *testing.T) {
	var items = []*gildedrose.Item{
		{"foo", 0, 0},
	}

	gildedrose.UpdateQuality(items)

	if items[0].Name != "foo" {
		t.Errorf("Name: Expected %s but got %s ", "foo", items[0].Name)
	}
}

func Test_SellDate_Quality_Degrade(t *testing.T) {
	var items = []*gildedrose.Item{
		{"Foo", 0, 2},
	}
	gildedrose.UpdateQuality(items)

	if items[0].Quality != 0 {
		t.Errorf("Quality: Expected %d but got %d ", 1, items[0].Quality)
	}
}

func Test_Aged_Brie_Goes_Up_In_Quality(t *testing.T) {
	var items = []*gildedrose.Item{
		{"Aged Brie", 5, 2},
	}
	gildedrose.UpdateQuality(items)

	if items[0].Quality != 3 {
		t.Errorf("Quality: Expected %d but got %d ", 0, items[0].Quality)
	}
}
func Test_SellDate_Quality_Non_Negative(t *testing.T) {
	var items = []*gildedrose.Item{
		{"Foo", 0, 0},
	}
	gildedrose.UpdateQuality(items)

	if items[0].Quality != 0 {
		t.Errorf("Quality: Expected %d but got %d ", 0, items[0].Quality)
	}
}

func Test_Quality_Not_More_Than_50(t *testing.T) {
	var items = []*gildedrose.Item{
		{"Aged Brie", 5, 50},
	}
	gildedrose.UpdateQuality(items)

	if items[0].Quality != 50 {
		t.Errorf("Quality: Expected %d but got %d ", 0, items[0].Quality)
	}
}

func Test_Quality_Not_More_Than_50_Item_Defined_Higher_Than_50(t *testing.T) {
	var items = []*gildedrose.Item{
		{"bob", 5, 80},
	}
	gildedrose.UpdateQuality(items)

	if items[0].Quality != 80 {
		t.Errorf("Quality: Expected %d but got %d ", 80, items[0].Quality)
	}
}
func Test_Sulfuras_Attr(t *testing.T) {
	var items = []*gildedrose.Item{
		{"Sulfuras, Hand of Ragnaros", 50, 50},
	}
	gildedrose.UpdateQuality(items)
	if items[0].Quality != 50 || items[0].SellIn != 50 {
		t.Errorf("Quality: Expected %d but got %d, Quantity: Expected %d but got %d ", 50, items[0].Quality, 50, items[0].SellIn)
	}
}

func Test_Backstage_10_Days(t *testing.T) {
	var items = []*gildedrose.Item{
		{"Backstage passes to a TAFKAL80ETC concert", 10, 20},
	}
	gildedrose.UpdateQuality(items)
	if items[0].Quality != 22 {
		t.Errorf("Quality: Expected %d but got %d ", 22, items[0].Quality)
	}
}

func Test_Backstage_5_Days(t *testing.T) {
	var items = []*gildedrose.Item{
		{"Backstage passes to a TAFKAL80ETC concert", 5, 20},
	}
	gildedrose.UpdateQuality(items)
	if items[0].Quality != 23 {
		t.Errorf("Quality: Expected %d but got %d ", 23, items[0].Quality)
	}
}

func Test_Backstage_After_Concert(t *testing.T) {
	var items = []*gildedrose.Item{
		{"Backstage passes to a TAFKAL80ETC concert", 0, 20},
	}
	gildedrose.UpdateQuality(items)
	if items[0].Quality != 0 {
		t.Errorf("Quality: Expected %d but got %d ", 0, items[0].Quality)
	}
}

func Test_Conjured_Items(t *testing.T) {
	var items = []*gildedrose.Item{
		{"Conjured", 5, 2},
	}
	gildedrose.UpdateQuality(items)

	if items[0].Quality != 0 {
		t.Errorf("Quality: Expected %d but got %d ", 0, items[0].Quality)
	}
}
