package gildedrose

type Item struct {
	Name            string
	SellIn, Quality int
}

func UpdateQuality(items []*Item) {
	for i := 0; i < len(items); i++ {

		if items[i].Name != "Aged Brie" && items[i].Name != "Backstage passes to a TAFKAL80ETC concert" && items[i].Name != "Conjured" {
			if items[i].Quality > 0 && items[i].Quality < 50 {
				if items[i].Name != "Sulfuras, Hand of Ragnaros" {
					items[i].Quality = items[i].Quality - 1
				}
			}
		} else if items[i].Name == "Conjured" {
			items[i].Quality = items[i].Quality - 2
		} else {
			if items[i].Quality < 50 {
				items[i].Quality = items[i].Quality + 1
				if items[i].Name == "Backstage passes to a TAFKAL80ETC concert" {
					if items[i].SellIn < 11 {
						if items[i].Quality < 50 {
							items[i].Quality = items[i].Quality + 1
						}
					}
					if items[i].SellIn < 6 {
						if items[i].Quality < 50 {
							items[i].Quality = items[i].Quality + 1
						}
					}
				}
			} else {
				items[i].Quality = 50
			}
		}

		if items[i].Name != "Sulfuras, Hand of Ragnaros" && items[i].SellIn > 0 {
			items[i].SellIn = items[i].SellIn - 1
		}

		if items[i].SellIn <= 0 {
			if items[i].Name != "Aged Brie" {
				if items[i].Name != "Backstage passes to a TAFKAL80ETC concert" {
					if items[i].Quality > 0 {
						if items[i].Name != "Sulfuras, Hand of Ragnaros" {
							items[i].Quality = items[i].Quality - 1
						}
					} else if items[i].Quality <= 0 {
						items[i].Quality = 0
					}
				} else {
					items[i].Quality = 0
				}
			}
		}
	}

}
